package com.fizzbuzz;

public class FizzBuzz {
    private final int fizz;
    private final int buzz;
    private final double limit;

    public FizzBuzz(int fizz, int buzz, double limit) {
        this.fizz = fizz;
        this.buzz = buzz;
        this.limit = limit;
    }

    public FizzBuzz(double limit) {
        this(3, 5, limit);
    }

    public FizzBuzz() {
        this(3, 5, 30);
    }

    public void start() {
        FizzBuzz.generateFizz(this.fizz, this.buzz, this.limit);
    }

    public static void generateFizz(int fizz, int buzz, double limit) {
        final String fizzWord = "fizz";
        final String buzzWord = "buzz";

        for (int i = 1; i <= limit; i++) {
            String output;

            if (i % fizz == 0 && i % buzz == 0) {
                output = fizzWord + " " + buzzWord;
            } else if (i % fizz == 0) {
                output = fizzWord;
            } else if (i % buzz == 0) {
                output = buzzWord;
            } else {
                output = String.valueOf(i);
            }

            System.out.println(output);
        }
    }

    public int getFizz() {
        return fizz;
    }

    public int getBuzz() {
        return buzz;
    }

    public double getLimit() {
        return limit;
    }
}
