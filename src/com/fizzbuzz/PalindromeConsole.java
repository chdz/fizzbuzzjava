package com.fizzbuzz;
import java.util.Scanner;

public class PalindromeConsole {
    String phrase;

    public void checkPhrase() {
        Palindromes palindromChecker = new Palindromes();

        getUserInput();
        System.out.println("is " + this.phrase + " a palindrome ? => " + palindromChecker.isPalindrome(this.phrase));
    }

    private void getUserInput() {
        Scanner userInput = new Scanner(System.in);

        System.out.println("What phrase do you want to check? :");
        this.phrase = userInput.next();
    }
}
