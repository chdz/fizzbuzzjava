package com.fizzbuzz;

import java.util.Arrays;

public class Palindromes {
    public static void checkLocalPhrases() {
        String phrases[] = {
                "fizz buzz",
                "kenzan",
                "race car",
                "this is a test",
                "anna",
                "stairway to heaven",
                "name no one man"
        };

        checkPalindromes(phrases);
    }

    public static void checkPalindromes(String phrases[]) {
        Arrays
          .stream(phrases)
          .forEach(phrase -> System.out.println("Is " + phrase + " a palindrome? => " + isPalindrome(phrase)));
    }

    private static String reverse (String phrase) {
        return new StringBuffer(phrase).reverse().toString();
    }

    public static boolean isPalindrome(String first) {
        first = first.toLowerCase();
        String second = reverse(first).replaceAll("\\s+","");
        first = first.replaceAll("\\s+","");

        return first.equals(second);
    }
}