package com.fizzbuzz;

import java.util.Scanner;

public class FizzBuzzConsole {
    int fizz;
    int buzz;
    double limit;

    public void startConsole() {
        Scanner userInput = new Scanner(System.in);

        this.getInput(userInput);
        this.getFizzBuzz(this.fizz, this.buzz, this.limit);
    }

    private void getInput(Scanner userInput) {
        this.setFizz(userInput);
        this.setBuzz(userInput);
        this.setMax(userInput);
    }

    private void setFizz(Scanner userInput) {
        System.out.println("Enter Fizz value: ");
        this.fizz = userInput.nextInt();
    }

    private void setBuzz(Scanner userInput) {
        System.out.println("Enter Buzz value: ");
        this.buzz = userInput.nextInt();
    }

    private void setMax(Scanner userInput) {
        System.out.println("Enter FizzBuzz upper boundary: ");
        this.limit = userInput.nextInt();
    }

    public void getFizzBuzz(int fizz, int buzz, double limit) {
        FizzBuzz fizzbuzz = new FizzBuzz(fizz, buzz, limit);

        System.out.println("====================================");
        fizzbuzz.start();
        System.out.println("====================================");
    }
}
