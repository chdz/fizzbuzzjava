package Test;

import com.fizzbuzz.Palindromes;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class PalindromeTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
        System.setErr(System.err);
    }

    @Test
    public void testDefaultPhrases() {
        String expectedOutput = "Is fizz buzz a palindrome? => false\n" +
                "Is kenzan a palindrome? => false\n" +
                "Is race car a palindrome? => true\n" +
                "Is this is a test a palindrome? => false\n" +
                "Is anna a palindrome? => true\n" +
                "Is stairway to heaven a palindrome? => false\n" +
                "Is name no one man a palindrome? => true";

        Palindromes testPalindromes = new Palindromes();
        testPalindromes.checkLocalPhrases();

        Assert.assertEquals(expectedOutput.trim(), outContent.toString().trim());
    }

    @Test
    public void checkPalindromes() {
        String[] testPhrases = {"home", "Bear", "Kenzan", "race car", "anna", "test"};
    }
}
