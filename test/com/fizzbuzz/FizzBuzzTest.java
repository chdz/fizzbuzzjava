package com.fizzbuzz;
import org.junit.Assert;
import org.junit.Test;

public class FizzBuzzTest {
    @Test
    public void shouldInitializeVariablesWhenFullContructor() {
        final int expectedFizz = 2;
        final int expectedBuzz = 4;
        final double expectedLimit = 20;

        FizzBuzz tester = new FizzBuzz(2, 4, 20);

        Assert.assertEquals(tester.getFizz(), expectedFizz);
        Assert.assertEquals(tester.getBuzz(), expectedBuzz);
        Assert.assertEquals(tester.getLimit(), expectedLimit, 0.001);
    }

    @Test
    public void shouldInitializeVariablesWhenOnlyLimitIsPassed() {
        final int defaultFizz = 3;
        final int defaultdBuzz = 5;
        final double expectedLimit = 20;

        FizzBuzz tester = new FizzBuzz(20);

        Assert.assertEquals(tester.getFizz(), defaultFizz);
        Assert.assertEquals(tester.getBuzz(), defaultdBuzz);
        Assert.assertEquals(tester.getLimit(), expectedLimit, 0.001);
    }

    @Test
    public void shouldInitializeVariablesWhenNoArgsArePassed() {
        final int defaultFizz = 3;
        final int defaultdBuzz = 5;
        final double defaultLimit = 30;

        FizzBuzz tester = new FizzBuzz();

        Assert.assertEquals(tester.getFizz(), defaultFizz);
        Assert.assertEquals(tester.getBuzz(), defaultdBuzz);
        Assert.assertEquals(tester.getLimit(), defaultLimit, 0.001);
    }
}
